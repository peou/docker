docker run -d \
  --name devtest \
  --mount source=myvol2,target=/app \
  nginx:latest

docker service create -d \
  --replicas=4 \
  --name devtest-service \
  --mount source=myvol2,target=/app \
  nginx:latest

docker service ps devtest-service
docker service rm devtest-service

docker run -d \
  --name=nginxtest \
  --mount source=nginx-vol,destination=/usr/share/nginx/html \
  nginx:latest